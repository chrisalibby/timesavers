$company = "YOUR COMPANY"
$to = who@where.com
$from = who@where.com
$server = mail.where.com
$vpngroup = 'Group Name'
$searchBase = "OU=dept,DC=domain,DC=local"

Remove-Item 'audit-user-report.csv'
$users = Get-ADUser -Filter {Enabled -eq $true} -SearchBase $searchBase 

Add-Content 'audit-user-report.csv' ('"' + $company + ' User Report"')
Add-Content 'audit-user-report.csv' ('"Total Count: ' + $users.count + '"')
Add-Content 'audit-user-report.csv' ('')
Add-Content 'audit-user-report.csv' ('"First Name","Last Name","Email","Telephone","Title","Extension","Department"')

ForEach ($user in $users) {
	$userdetail = Get-ADUser -Identity $user.samaccountname -Properties GivenName, Surname, Mail, Pager, Title, telephoneNumber, Department
	Add-Content 'audit-user-report.csv' ('"'+$user.GivenName+'","'+$user.Surname+'","'+$userdetail.Mail+'","'+$userdetail.Pager+'","'+$userdetail.Title+'","'+$userdetail.telephoneNumber+'","'+$userdetail.Department+'"')
}

#Send-MailMessage -To $to -From $from -Subject 'VPN User Report' -Body 'Review for inactive users, incorrect assignment, and proper change management.' -Attachments 'audit-user-report.csv' -SmtpServer $server
