#Import Modules
Import-module activedirectory
#----------------------------------------------------------------
# User-Defined Variables - may change to parameters in the future
# OUs to search in
# Example - one OU
#$ous = 'OU=Users,DC=domain,DC=local','OU=Contractors,DC=domain,DC=local','OU=Vendors,DC=domain,DC=local'
# Example - multiple OU
$ous = 'OU=Users,DC=domain,DC=local'
#----------------------------------------------------------------
# Functions
#----------------------------------------------------------------
#Get Manager in human readable format
function getmanagerfromdn
{
  param([string]$managerdn)
  # Have to grab the manager name with another lookup
  # Need to check if lookup is null to prevent error
  if ([string]::IsNullOrEmpty($managerdn) -ne $true)
    {
      $manager = $(Get-ADUser -Identity $managerDN).name
    } else {
      $manager = "N/A"
    }
  return $manager
}
#Get Groups for a user in a human-readable format (long string)
function getgroups
{
  param([string]$userid)
  # Have to grab the manager name with another lookup
  # Need to check if lookup is null to prevent error
  if ([string]::IsNullOrEmpty($userid) -ne $true)
    {
#      $groups = $(Get-ADUser -Identity $userdetail.Manager).name
        $grps = get-aduser $userid -Properties memberof
        foreach ($grpo in $grps.memberof) {
            $grp = (get-adgroup $grpo).name
            #Add Group Name to Group Array
            $groups += "$grp`n"
        }
        $groups = $groups.Substring(0,$groups.Length-1)

    } else {
      $groups = "N/A"
    }
  return $groups
}
#----------------------------------------------------------------
# Main script
#----------------------------------------------------------------
# Empty Array
$Array = @()
# Get your users and start a loop
$Users = $ous | foreach-object { get-aduser -filter * -searchbase $PSItem -Properties Enabled, lockedout, `
   Manager, description, Department, LastLogonDate, LastBadPasswordAttempt, passwordLastSet, passwordNeverExpires, `
   PasswordNotRequired, PasswordExpired } | ForEach-Object ({
    # Make a new object with the findings
    $NewObj = New-Object System.Object
    $NewObj | Add-Member -type NoteProperty -Name "Name" -Value $_.Name
    $NewObj | Add-Member -type NoteProperty -Name "Description" -Value $_.description
    $NewObj | Add-Member -type NoteProperty -Name "Department" -Value $_.department
    $NewObj | Add-Member -type NoteProperty -Name "Manager" -Value $(getmanagerfromdn($_.Manager))
    $NewObj | Add-Member -type NoteProperty -Name "Account" -Value $_.samaccountname
    $NewObj | Add-Member -type NoteProperty -Name "Enabled" -Value $_.Enabled
    $NewObj | Add-Member -type NoteProperty -Name "Locked Out" -Value $_.LockedOut
    $NewObj | Add-Member -type NoteProperty -Name "Last Logon" -Value $_.LastLogonDate
    $NewObj | Add-Member -type NoteProperty -Name "Last Bad Password" -Value $_.LastBadPasswordAttempt
    $NewObj | Add-Member -type NoteProperty -Name "Password Last Set" -Value $_.passwordLastSet
    $NewObj | Add-Member -type NoteProperty -Name "Password Never Expires" -Value $_.passwordNeverExpires
    $NewObj | Add-Member -type NoteProperty -Name "Password Not Required" -Value $_.PasswordNotRequired
    $NewObj | Add-Member -type NoteProperty -Name "Password Expired" -Value $_.PasswordExpired
    $NewObj | Add-Member -type NoteProperty -Name "Groups" -Value $(getgroups($_.samaccountname))
    # Add the object to an array (one at a time)
    $Array += $NewObj
})
# End Loop# Dump filled array to a file
# ToDo: Make output file user-input from parameter
$Array | Sort-Object -Property Department, Name | export-csv "UserReview.csv" -NoTypeInformation
