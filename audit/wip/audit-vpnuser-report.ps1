$company = "YOUR COMPANY"
$to = who@where.com
$from = who@where.com
$server = mail.where.com
$vpngroup = 'Group Name'

Remove-Item 'audit-vpnuser-report.csv'
$users = Get-ADGroupMember -Identity $vpngroup
#$users | Select-Object name,samaccountname | Export-CSV -Path 'audit-vpnuser-report.csv' -NoTypeInformation

Add-Content 'audit-vpnuser-report.csv' ('"' + $company + ' Domain Admins Report"')
Add-Content 'audit-vpnuser-report.csv' ('"Total Count: ' + $users.count + '"')
Add-Content 'audit-vpnuser-report.csv' ('')
Add-Content 'audit-vpnuser-report.csv' ('"User","Username","Enabled","description","Department","Manager"')

ForEach ($user in $users) {
	$userdetail = Get-ADUser -Identity $user.samaccountname -Properties Enabled, Manager, description, Department
	Add-Content 'audit-vpnuser-report.csv' ('"'+$user.name+'","'+$user.samaccountname+'","'+$userdetail.enabled+'","'+$userdetail.description+'","'+$userdetail.department+'","'+$userdetail.manager+'"')
}

Send-MailMessage -To $to -From $from -Subject 'VPN User Report' -Body 'Review for inactive users, incorrect assignment, and proper change management.' -Attachments 'audit-vpnuser-report.csv' -SmtpServer $server
