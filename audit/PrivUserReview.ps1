# Remove the old copy
# ToDo: made the file name user-selectable via parameter
Remove-Item ./privusers.csv
# Select all users in default priv. groups, then grab details and dump to file
'Account Operators', 'Administrators', 'Backup Operators', 'Domain Admins', 'Enterprise Admins', 'Print Operators', 'Schema Admins', 'Server Operators' | ForEach-Object {
    $groupName = $_
    Get-ADGroupMember -Identity $_ -Recursive | Get-ADUser -Properties Enabled, lockedout, Manager, description, Department, LastLogonDate, LastBadPasswordAttempt, passwordLastSet, passwordNeverExpires, PasswordNotRequired, PasswordExpired | Select-Object Name, @{n='GroupName';e={ $groupName }}, Description, Enabled, LockedOut, LastLogonDate, LastBadPasswordAttempt, passwordLastSet, passwordNeverExpires, PasswordNotRequired, PasswordExpired | Export-Csv -Append -Force -Path .\privusers.csv -NoTypeInformation
}
# Open the file in default CSV handler
Invoke-Item ./privusers.csv
