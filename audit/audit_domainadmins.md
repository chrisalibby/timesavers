# Audit of Domain Admins
It is recommended that the Domain Admins be reviewed on a regular basis.
**Need to get supporting info for this.**

## Recommended Procedure
- Run powershell\audit-domainadmin-report.ps1 on at least annual basis.  Recommend quarterly.
- Review the report.  Note potential conflicts.
- Review sign-off should be different than approval sign off.

## Issues to watch For
- Actively used well-known accounts/SIDs
  - Attackers are going to try default usernames first before moving.  You have basically given away one part of the two-part key.
- Daily use accounts with Domain Admin rights (especially with mailboxes)
  - Increased risk of accidental malware infection spreading across the domain, or RAT gaining access to entire contents of the domain.
  - Need evidence for this - MITRE ATT&CK?
- Shared accounts
  - Shared accounts violate segregation of duties principle and typically lead to violation of least priviledge principle.
- Accounts that allow no passwords
- Accounts that allow no password expiration
  - Can be OK by should implement manual password rotation on these accounts.
