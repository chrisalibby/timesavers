<#

.SYNOPSIS
    audit-aduser-report.ps1 - Generates a CSV report containing the users of Active Directory Domain

.DESCRIPTION
    Generates a CSV report containing the members of the Domain Admins AD group.
    The report can either be saved to a path or emailed via a mail server.

.PARAMETER OutputFile
    File name (including path if different than current) to write the report to

.PARAMETER Server
    Mail Server hostname or IP address
    NOTE: Requires -to and -from

.PARAMETER To
    Email recipient

.PARAMETER From
    Email sender

.PARAMETER Company
    Adds the specified company name to the report header.

.PARAMETER Enabled
    Selects only enabled accounts.

.PARAMETER Privileged
    Selects only accounts which have administrative rights on the domain.
    NOT YET IMPLIMENTED

.PARAMETER SearchBase
    Set the OU to search within (Use DN format)

.EXAMPLE
    C:\PS> audit-aduser-report.ps1
    Outputs the report to the default file name.DESCRIPTION

    C:\PS> audit-aduser-report.ps1 -outputfile thisfilehere.csv
    Outputs the report to the specified file name.

    C:\PS> audit-aduser-report.ps1 -server mail.server.com -to recipient@recipient.com -from sender@sender.net
    Outputs the report to a temp file and emails the results as an attachment.

.NOTES
    Author: Chris Libby
    Date:   April 8, 2019

#>
[CmdletBinding()]
Param(
    [Parameter(HelpMessage="Company name for report")][ValidateNotNullOrEmpty()][string]$company = "YOUR COMPANY",
    [Parameter(HelpMessage="File path & name for output")][ValidateNotNullOrEmpty()][string]$outputfile,
    [Parameter(HelpMessage="Mail Server Hostname/IP")][ValidateNotNullOrEmpty()][string]$server,
    [Parameter(HelpMessage="Send report to")][ValidateNotNullOrEmpty()][string]$to,
    [Parameter(HelpMessage="From Address for email")][ValidateNotNullOrEmpty()][string]$from,
    [Parameter(HelpMessage="SearchBase DN")][string]$searchbase,
    [Parameter(HelpMessage="Overwrite Existing?")][switch]$overwrite,
    [Parameter(HelpMessage="Only Enabled Accounts?")][switch]$enabled,
    [Parameter(HelpMessage="Only Privileged Accounts?")][switch]$privileged
)
#
#------------------------------------
function getmanagerfromdn
{
  param([string]$managerdn)
  # Have to grab the manager name with another lookup
  # Need to check if lookup is null to prevent error
  if ([string]::IsNullOrEmpty($managerdn) -ne $true)
    {
      $manager = $(Get-ADUser -Identity $userdetail.Manager).name
    } else {
      $manager = "N/A"
    }
  return $manager
}
#------------------------------------
# Moved from hard-coded file to temp file name generation
#remove tempfile logic in favor of picking a filename
#$filename = [System.IO.Path]::GetTempPath() + [GUID]::NewGuid().ToString() + ".csv"
If ([string]::IsNullOrEmpty($outputfile))
{
  If ([string]::IsNullOrEmpty($server))
  {
    $filename = [GUID]::NewGuid().ToString() + ".csv"
  } else {
    $filename = [System.IO.Path]::GetTempPath() + [GUID]::NewGuid().ToString() + ".csv"
  }
} else {
  $filename = $outputfile
}
If ((Test-Path($filename)) -And $overwrite -eq $false)
{
  Throw "$filename exists.  Use the -overwrite switch to overwrite it."
}
If (Test-Path $filename) {
  Remove-Item -Path $filename
}

# TODO: Logic here to determine if all of AD, specific OU, specific group
# set the base filter first so we at least have that
$users_filter = '*'
if ($enabled) {
  $users_filter = 'enabled -eq $true'
}

# TODO: Logic to get privileged users only
#  'Domain Admins', 'Administrators', 'Enterprise Admins', 'Schema Admins', 'Server Operators', 'Backup Operators'
#$users = Get-ADGroupMember -Identity 'Domain Admins'
#if ($privileged) {
#  'Domain Admins', 'Administrators', 'Enterprise Admins', 'Schema Admins', 'Server Operators', 'Backup Operators' | ForEach-Object {
#      $groupName = $_
#      Get-ADGroupMember -Identity $_ -Recursive | Get-ADUser | Select-Object Name, DisplayName, @{n='GroupName';e={ $groupName }}
#  }
#}

# TODO: logic to set search base
if ([string]::IsNullOrEmpty($searchbase)) {
    $users = Get-ADUser -filter $users_filter
} else {
    $users = Get-ADUser -filter $users_filter -SearchBase $searchbase
}

Add-Content $filename ('"' + $company + ' Active Directory User Audit Report"')
Add-Content $filename ('"Total Count: ' + $users.count + '"')
Add-Content $filename ('"Report Date: ' + $(Get-Date -Format g) + '"')
Add-Content $filename ('')
Add-Content $filename ('"User","Username","Enabled","Locked Out","description","Department","Manager","Last Login","Last Bad Login","Password Expired","Password Last Set","Password Never Expires","PasswordNotRequired"')

# Loop thru the results to generate the output
ForEach ($user in $users) {
  # clear out loop temp vars
  $userdetail = $manager = $Null
  # grab the dataset from AD
	$userdetail = Get-ADUser -Identity $user.samaccountname -Properties Enabled, lockedout, Manager, description, Department, LastLogonDate, LastBadPasswordAttempt, passwordLastSet, passwordNeverExpires, PasswordNotRequired, PasswordExpired
  # Have to grab the manager name with another lookup
  $manager = getmanagerfromdn($userdetail.Manager)
  # output results to the file
	Add-Content $filename ('"' + $user.name + '","' `
  + $user.samaccountname + '","' `
  + $userdetail.enabled + '","' `
  + $userdetail.lockedout + '","' `
  + $userdetail.description + '","' `
  + $userdetail.department + '","' `
  + $manager + '","' `
  + $userdetail.LastBadPasswordAttempt + '","' `
  + $userdetail.LastLogonDate + '","' `
  + $user.PasswordExpired + '","' `
  + $user.PasswordLastSet + '","' `
  + $user.PasswordNeverExpires + '","' `
  + $user.PasswordNotRequired + '"')
}

# only send the email is the email server is specified
if (-Not ([string]::IsNullOrEmpty($server)))
{
  Send-MailMessage -To $to -From $from -Subject 'Active Directory User Audit Report' -Body 'Review for inactive users, incorrect assignment, and proper change management.' -Attachments $filename -SmtpServer $server
  Remove-Item -Path $filename
}
